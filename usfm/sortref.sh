#!/bin/bash
grep -o '[0-3A-Z][A-Za-z ]*\. [0-9]*,[0-9]' *.usfm >out
sed -ri 's/ [0-9]*,[0-9]//g' out
sed -ri 's/^[A-Z1-3a-z]*\.usfm://g' out
sort out >out2
uniq out2 >ref
rm out out2
